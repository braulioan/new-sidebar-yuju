import React from 'react';
import './App.css';
import ProSideBar from '../src/components/ProSideBar';

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <ProSideBar></ProSideBar>
      </div>
    );
  }
}

export default App;
