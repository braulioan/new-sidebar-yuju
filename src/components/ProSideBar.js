import React, { Component } from 'react';
import logo from './img/logo-white.png';
import $ from 'jquery';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/prosidebar.css';
import './css/theme.css';

class ProSideBar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            hover: false
        };
    }
    componentDidMount(){
        $('.sidebar-dropdown > a').click(function () {
            $('.sidebar-submenu').slideUp(100);
            if ($(this).parent().hasClass('active')) {
                $('.sidebar-dropdown').removeClass('active');
                $(this).parent().removeClass('active');
            } else {
                $('.sidebar-dropdown').removeClass('active');
                $(this).next('.sidebar-submenu').slideDown(200);
                $(this).parent().addClass('active');
            }
        });
    }

    handleHover = () => {
        this.setState({
            hover: !this.state.hover
        })
    }

    render() {
        const hovered = this.state.hover ? 'sidebar-hovered' : ''
        return (
            <div className={`page-wrapper legacy-theme boder-radius-on toggled pinned ${hovered}`}>
                <nav id='sidebar' className='sidebar-wrapper' onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}>
                    <div className='sidebar-content'>
                        <div className='sidebar-item sidebar-brand'>
                            <img className='img-responsive img-rounded' style={{width : '100px'}} src={logo} alt=''/>
                        </div>
                        <div className='sidebar-item sidebar-menu'>
                            <ul>
                                <li className='sidebar-dropdown'>
                                    <a href='#'>
                                        <i>
                                            {/* ICON */}
                                        </i>
                                        <span className='menu-texts'>Yuju marketplace</span>
                                    </a>
                                    <div className='sidebar-submenu'>
                                        <ul>
                                            <li className='dropdown'>
                                                <a>Tienda de Braulio</a>
                                                <div className='dropdown-content'>
                                                    <a href="#">Otra Tienda</a>
                                                </div>
                                            </li>
                                            <li>
                                                <a href='#'>Añadir productos
                                                </a>
                                            </li>
                                            <li>
                                                <a href='#'>Mis productos</a>
                                            </li>
                                            <li>
                                                <a href='#'>Configuración</a>
                                            </li>
                                            <li>
                                                <a href='#'>Lanzar</a>
                                            </li>
                                            <li>
                                                <a href='#'>Ventas</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li className='sidebar-dropdown'>
                                    <a href='#'>
                                        <i>
                                            {/* ICON */}
                                        </i>
                                        <span className='menu-text'>Yuju mensajería</span>
                                    </a>
                                    <div className='sidebar-submenu'>
                                        <ul>
                                            <li>
                                                <a href='#'>Preguntas</a>
                                            </li>
                                            <li>
                                                <a href='#'>Configuración</a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>

                                <li>
                                    <a href='#'>
                                        <i>
                                            {/* ICON */}
                                        </i>
                                        <span className='menu-text'>Mi cuenta</span>
                                    </a>
                                </li>

                                <div className='links-down'></div>

                                <li>
                                    <a href='#'>
                                        <i>
                                            {/* ICON */}
                                        </i>
                                        <span className='menu-text'>Notificaciones</span>
                                    </a>
                                </li>
                                
                                <li className='sidebar-dropdown'>
                                    <a>
                                        <i>
                                            {/* <FontAwesomeIcon icon={faShoppingCart} /> */}
                                        </i>
                                        <span className='menu-text'>Cuentas</span>
                                    </a>

                                    <div className='sidebar-submenu'>
                                        <ul>
                                            <li>
                                                <a href=''>Otra cuenta</a>
                                            </li>
                                            <li>
                                                <a href=''>Otra cuenta</a>
                                            </li>
                                            
                                        </ul>
                                    </div>
                                </li>
                                    
                                <li>
                                    <a href='#'>
                                        <i>
                                            {/* ICON */}
                                        </i>
                                        <span className='menu-text'>Salir</span>
                                    </a>
                                </li>
                            
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

export default ProSideBar;